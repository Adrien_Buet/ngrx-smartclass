# NgrxSmartclass

## Test the application

* Install app packages with command `npm install`
* Run the app with `npm start`
* Browse the app on `localhost:4200`
* The `Load all users` button should load and display a bunch of users
* The `Search users` button is not yet working
* Open Chrome debugging tool and look at the Redux tab while using the app

## Browse code
* All the NgRx stuff (actions, reducers, effects, selectors) is located into `src/app/store`
* The `src\app\services` contains the HTTP service making the API calls
* The `src\app\components` contains the UI elements and binds the view with the store


## Implement the search

* Create a new action in the user actions to handle search queries
  * It should have a property that will contain search text (Look at Success or Failure action for how to add property to the action)
  * Note: we will re-use the same Success/Error actions to handle search API results
* In the user reducer, handle to new action properly
* In the user effects, add a new effects to handle the ation
  * Add a new field to class `UserEffects` and instanciate it using the `createEffect` method. The field can have any name.
  * Call the `search` method instead of the `getAll` method in `userService`
  * The text to search should be retrieved from the action parameter of the `switchMap` method
  * Handle success and error similarly to the previous effect () Caution: The `search` method does not return directly an array of users)
* In the main page component (`main-page.component.ts`), implement correctly the `onSearchUsersClick` method to dispatch your new action

## Handle search errors
* When searching without any text, an error is returned by the API
* Store the error message in a new property of the user state
* Display it in the UI (use `{{ users.errorMessage }}` to display some text in HTML)

## Unit testing

* Run the test using command `npm test`. Some should fail.
* We are using `jasmine` library to run test. This is the default when creatin an angular project.

### Reducer
* Open `src\app\store\user\user.reducer.spec.ts` file. Each `it` function represent a test.
* Reducer functions are easy to test because they are pure function. For each input, we expect an specific output.
* The first 2 tests are already implemented. They test:
  * an unknown action. Every reducers of the app are called for each action. So if you want to ignore an action, just return the previous state.
  * the `loadUsers` action. We expect to set the status to `loading`
* Implement the last two tests

### Effects
* Open `src\app\store\user\user.effects.spec.ts`
* Effects are a bit harder to test. They are using the action stream as input. They often use other dependencies (that we are mocking using mockito-ts library). They return an action stream. We will use `jasmine-marbles` to test observables.
* Look at the first test. We use `jasmine-marbles` to create the input action stream. We then mock the user service to return a stream of `User[]`. Finally we compare the effect with the expected stream of action.
* Let's try to fix the error test. We want to test that the correct action stream is returned when the user service encounter an issue (network issue, 4xx/5xx answers by the server...)

### Components
* Open `src\app\componennts\main-page\main-page.component.spec.ts`
* Components are much harder to test because they are tightly coupled with Angular (HTML template binding, dependency injection) and UI interactions
* TestBed is an Angular tool to fake dependency injection and all the template binding mechanisms
* We use NgRx tool to mock the store and we use jasmine-spy to make sure that actions are correctly dispatched on click

### Add your own tests
* Try to add reducer and component test to test the "handle search error" feature that we previously created