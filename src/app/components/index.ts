import { MainPageComponent } from './main-page/main-page.component';
import { UserComponent } from './user/user.component';

export const components = [
  UserComponent,
  MainPageComponent
];
