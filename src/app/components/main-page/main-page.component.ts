import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

import { State } from '../../store/reducers';
import { loadUsers } from '../../store/user/user.actions';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent {
  search = '';

  constructor(private readonly store: Store<State>) { }

  onLoadUsersClick() {
    this.store.dispatch(loadUsers());
  }

  onSearchUsersClick() {
    alert(`Serching for ${this.search}`);
  }
}
