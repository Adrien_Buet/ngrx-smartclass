import { User } from 'src/app/services/user.service';

import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { State } from '../../store/reducers';
import { selectUsers } from '../../store/user/user.selectors';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  users$ = this.store.select(selectUsers);
  displayedColumns: string[] = ['id', 'login'];

  constructor(private readonly store: Store<State>) { }

  ngOnInit() { }

  userTrackBy(_, item: User) {
    return item.id;
  }
}
