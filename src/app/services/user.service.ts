import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private readonly http: HttpClient) { }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(
      `${environment.githubApiUrl}/users`
    );
  }

  search(search: string): Observable<SearchResult> {
    const url = `${environment.githubApiUrl}/search/users?q=${search}+in:login`;
    return this.http.get<SearchResult>(url);
  }
}

export interface User {
  readonly login: string;
  readonly id: number;
}

export interface SearchResult {
  readonly incomplete_result: boolean;
  readonly items: User[];
  readonly total_count: number;
}
