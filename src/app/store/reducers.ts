import {
    ActionReducer, ActionReducerMap, createFeatureSelector, createSelector, MetaReducer
} from '@ngrx/store';

import { environment } from '../../environments/environment';
import * as fromUser from './user/user.reducer';

export interface State {
  users: fromUser.State;
}

export const reducers: ActionReducerMap<State> = {
  users: fromUser.reducer
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
