import { cold, hot } from 'jasmine-marbles';
import { Observable, of } from 'rxjs';
import { instance, mock, when } from 'ts-mockito';

import { TestBed } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';

import { UserService } from '../../services/user.service';
import { loadUsers, loadUsersFailure, loadUsersSuccess } from './user.actions';
import { UserEffects } from './user.effects';

describe('UserEffects', () => {
  let actions$: Observable<any>;
  let effects: UserEffects;
  let userService: UserService;

  beforeEach(() => {
    userService = mock(UserService);
    TestBed.configureTestingModule({
      providers: [
        UserEffects,
        { provide: UserService, useValue: instance(userService) },
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get<UserEffects>(UserEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  describe('load user effect', () => {
    it('should return all users', () => {
      /* Special syntax to create an observable. Each character is a 'tick' (10ms) in time.
      Here, the observable emits an action at the third tick.
      Hot means that the observable is already emitting before any subscription is made on the effect
      (VS cold where emition starts at subscription)*/
      actions$ = hot('--a-', { a: loadUsers() });
      const users = [{ id: 12, login: 'admin' }];
      when(userService.getAll()).thenReturn(cold('-a', { a: users }));

      const expected = cold('---a', { a: loadUsersSuccess({ data: users }) });
      expect(effects.loadUsers$).toBeObservable(expected);
    });

    it('should return an error in case of issue', () => {
      actions$ = hot('--a-', { a: loadUsers() });
      const err = new Error('Cannot load users');
      /* # syntaxt indicates taht an error occured on the stream during the tick.
      The error thrown can be passed throw the third argument of cold/hot functions*/
      when(userService.getAll()).thenReturn(cold('---#', {}, err));

      const expected = cold('', {});
      expect(effects.loadUsers$).toBeObservable(expected);
    });
  });
});
