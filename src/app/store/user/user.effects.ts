import { EMPTY, of } from 'rxjs';
import { catchError, concatMap, map, switchMap } from 'rxjs/operators';
import { UserService } from 'src/app/services/user.service';

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import * as UserActions from './user.actions';

@Injectable()
export class UserEffects {

  loadUsers$ = createEffect(() => this.actions$.pipe(
    ofType(UserActions.loadUsers),
    switchMap(action => this.userService.getAll().pipe(
      map(data => UserActions.loadUsersSuccess({ data })),
      catchError(error => of(UserActions.loadUsersFailure({ error })))
    )),
  ));

  constructor(private readonly actions$: Actions, private readonly userService: UserService) { }

}
