import { ExpectedConditions } from 'protractor';

import { loadUsers, loadUsersSuccess } from './user.actions';
import { initialState, reducer } from './user.reducer';

describe('User Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('load users', () => {
    it('should set loading state, remove errors and values', () => {
      const action = loadUsers();
      const previousState = { hasError: true, loading: false, values: [{ id: 1, login: 'abuet ' }] };

      const result = reducer(previousState, action);

      expect(result).toEqual({
        hasError: false,
        loading: true,
        values: [],
      });
    });
  });

  describe('load users success', () => {
    it('should unset loading state and store values', () => {
      const users = [{ id: 1, login: 'abuet' }, { id: 35, login: 'bbougot' }];
      const action = loadUsersSuccess({ data: users });

      const result = reducer(initialState, action);

      expect(result).toEqual(initialState);
    });
  });

  describe('load users failure', () => {
    it('should set loading state, remove errors and values', () => {
      expect(true).toBe(false);
    });
  });
});
