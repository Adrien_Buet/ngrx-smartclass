import { User } from 'src/app/services/user.service';

import { Action, createReducer, on } from '@ngrx/store';

import * as UserActions from './user.actions';

export const userFeatureKey = 'user';

export interface State {
  readonly loading: boolean;
  readonly hasError: boolean;
  readonly values: User[];
}

export const initialState: State = {
  loading: false,
  hasError: false,
  values: [],
};

const userReducer = createReducer(
  initialState,

  on(UserActions.loadUsers, state => ({
    ...state,
    loading: true,
    hasError: false,
    values: [],
  })),
  on(UserActions.loadUsersSuccess, (state, action) => ({
    ...state,
    loading: false,
    hasError: false,
    values: action.data,
  })),
  on(UserActions.loadUsersFailure, state => ({
    ...state,
    loading: false,
    hasError: true,
    values: [],
  })),

);

export function reducer(state: State | undefined, action: Action) {
  return userReducer(state, action);
}
