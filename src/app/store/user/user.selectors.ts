
import { State } from '../reducers';

export const selectUsers = (state: State) => state.users;
